// queue.c
// Copyright 2023.5.1 robin.rowe@cinepaint.org
// License open source MIT

#include <stdlib.h>
#include <stdio.h>
#include "queue.h"

 // #define VERBOSE

bool open_queue(Queue* queue,size_t size)
{	if(!queue)
	{	return false;
	}
	queue->front = queue->back = queue->count = queue->size = 0;
	if(!size)
	{	return false;
	}
	queue->data = malloc(sizeof(Data) * size);
	if(!queue->data)
	{	return false;
	}
	queue->size = size;
	return true;
}

bool push_queue(Data data,Queue* queue)
{	if(is_full_queue(queue))
	{	return false;
	}
	queue->data[queue->front] = data;
	queue->front++;
	if(queue->front >= queue->size)
	{	queue->front = 0;	
	}
	queue->count++;
#ifdef VERBOSE
	print_queue(queue);
#endif
	return true;
}

bool pop_queue(Data* data,Queue* queue)
{	if(is_empty_queue(queue))
	{	return false;
	}
	*data = queue->data[queue->back];
	queue->back++;
	if(queue->back >= queue->size)
	{	queue->back = 0;
	}
	queue->count--;
#ifdef VERBOSE
	print_queue(queue);
#endif
	return true;
}

bool is_full_queue(Queue* queue)
{	return queue->count >= queue->size;
}

bool is_empty_queue(Queue* queue)
{	return !queue->count;
}

void close_queue(Queue* queue)
{	free(queue->data);
	open_queue(queue,0);
}

void print_queue(Queue* queue)
{	size_t count = queue->count;
	size_t back = queue->back;
	printf("Queue[%zu] (%zu:%zu) ",count,queue->back, queue->front);
	while(count--)
	{	printf("%d ",queue->data[back]);
		back++;
		if(back >= queue->size)
		{	back = 0;
	}	}
	puts("");
}

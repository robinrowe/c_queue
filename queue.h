// queue.h
// Copyright 2023.5.1 robin.rowe@cinepaint.org
// License open source MIT

#ifndef queue_h
#define queue_h

#include <stdbool.h>
#include <stddef.h>

typedef int Data;

typedef struct Queue
{	Data* data;
	size_t front;
	size_t back;
	size_t count;
	size_t size;
} Queue;

bool open_queue(Queue* queue,size_t size);
bool push_queue(Data data,Queue* queue);
bool pop_queue(Data* data,Queue* queue);
bool is_full_queue(Queue* queue);
bool is_empty_queue(Queue* queue);
void close_queue(Queue* queue);
void print_queue(Queue* queue);

#endif

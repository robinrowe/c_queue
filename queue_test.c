// queue_test.c
// Copyright 2023.5.1 robin.rowe@cinepaint.org
// License open source MIT

#include <stdio.h>
#include "queue.h"

enum
{	OK,
	OPEN_FAIL,
	IS_EMPTY_FAIL,
	IS_FULL_FAIL
};

int main()
{	Queue queue;
	if(!open_queue(&queue,5))
	{	puts("Open failed");
		return OPEN_FAIL;
	}
	if(!is_empty_queue(&queue))
	{	puts("is empty failed");
		return IS_EMPTY_FAIL;
	}
	puts("Push 0 1 2");
	push_queue(0,&queue);
	push_queue(1,&queue);
	push_queue(2,&queue);
	print_queue(&queue);
	Data data;
	puts("Pop 0 1");
	pop_queue(&data,&queue);
	pop_queue(&data,&queue);
	print_queue(&queue);
	puts("Push 3 4 5 6 7");
	push_queue(3,&queue);
	push_queue(4,&queue);
	push_queue(5,&queue);
	push_queue(6,&queue);
	push_queue(7,&queue);
	print_queue(&queue);
	if(!is_full_queue(&queue))
	{	puts("is full failed");
		return IS_FULL_FAIL;
	}
	printf("Queue count = %zu\n",queue.count);
	close_queue(&queue);
	puts("Done");
	return OK;
}

/* output:

MacBook-Air build % ./queue_test
Push 0 1 2
Queue[1] (0:1) 0 
Queue[2] (0:2) 0 1 
Queue[3] (0:3) 0 1 2 
Queue[3] (0:3) 0 1 2 
Pop 0 1
Queue[2] (1:3) 1 2 
Queue[1] (2:3) 2 
Queue[1] (2:3) 2 
Push 3 4 5 6 7
Queue[2] (2:4) 2 3 
Queue[3] (2:0) 2 3 4 
Queue[4] (2:1) 2 3 4 5 
Queue[5] (2:2) 2 3 4 5 6 
Queue[5] (2:2) 2 3 4 5 6 
Queue count = 5
Done

*/